import EntryList from './../components/EntryList';
import EntryAdd from './../components/EntryAdd';
import { Container, Row, Col } from 'react-bootstrap'

export default function Entry () {

    return (  
      <Container margin="20px auto">
        <Row className="my-5">
          <Col>
            <h1>ENTRIES...</h1>
            <EntryAdd />
            <EntryList />
          </Col>
        </Row>
      </Container>
    )
}