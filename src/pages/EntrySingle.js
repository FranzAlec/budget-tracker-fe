import { useEffect,useState } from 'react';
import { Container,Row,Col,Card } from 'react-bootstrap';
import { useParams } from 'react-router-dom';
import EntryListUpdate from './../components/EntryListUpdate';


export default function EntrySingle () {

    const { id } = useParams()
    const [ entry, setEntry] = useState({
        categoryId: "",
        description: "",
        amount: ""
    })
    const[isLoading, setIsLoading] = useState(true)
    const [lastUpdatedEntry , setLastUpdatedEntry] = useState({})

    
    useEffect(() => {


        
		fetch(`https://budget-tracker-be.herokuapp.com/api/entries/${id}`,{
            headers: {
                "Authorization" : `Bearer ${localStorage.getItem('token')}`
            }
        })
		.then( res => res.json())
		.then( data => {
			// console.log(data)
           
            setEntry(data)
            setIsLoading(false)
		})
		.catch( err => console.log(err))
    },[lastUpdatedEntry])


   
    return (
        <Container>
            <Row>
                <Col xs={6} sm={6} md={6} className="mx-auto">
                    <Card.Body>
                        <h3>Update Entry</h3>
                        <Card.Title>Description: {entry.description}</Card.Title>
                        <Card.Title>Amount: {entry.amount}</Card.Title>
                    <Card.Title>Category: {entry.categoryId.name}</Card.Title>
                    </Card.Body>
                </Col>
                
            </Row>
                
            <Row>
                <Col>
                <EntryListUpdate entry={entry} setLastUpdatedEntry= {setLastUpdatedEntry} />
                </Col>  
            </Row>
        </Container>
    )

}