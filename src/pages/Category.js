import CategoryList from './../components/CategoryList'
import { Container, Row, Col } from 'react-bootstrap'
import CategoryAdd from '../components/CategoryAdd'



export default function Category() {

    
	return(
		<Container>
          <Row className="my-5">
            <h1  className="categorytitle  ml-3">CATEGORIES...</h1>
            <Col xs={12} sm={12} md={12}>
              <CategoryAdd />
            </Col>
            <Col xs={12} sm={12} md={12}>
              <CategoryList />
            </Col>
          </Row>
    </Container>
	)

}
