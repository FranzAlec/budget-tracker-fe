import { Redirect } from 'react-router-dom';
import { useState } from 'react';
import { Container, Row, Col } from 'react-bootstrap';

import RegistrationForm from './../components/RegistrationFrom';

export default function Register () {

    //create useState for redirection
    const [ isRedirect, setIsRedirect ] = useState (false)

    return (

         // if is redirect true >> homepage
         isRedirect ?
         <Redirect to="/login" /> :
 
         <Container>
             <Row>
                 <Col>
                    <h1>SignUp</h1>
                    <RegistrationForm setIsRedirect={setIsRedirect} />
                 </Col>
             </Row>
         </Container>
        

    )
}