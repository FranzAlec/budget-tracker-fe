import { useEffect,useState } from 'react';
import { Container,Row,Col,Card } from 'react-bootstrap';
import { useParams } from 'react-router-dom';
import CategoryListDisplay from './../components/CategoryListDisplay';
import CategoryListUpdate from './../components/CategoryListUpdate';


export default function CategorySingle () {

    const { id } = useParams()
    const [ category, setCategory] = useState([{}])
    const[isLoading, setIsLoading] = useState(true)
    const [lastUpdatedCategory , setLastUpdatedCategory] = useState({})

    useEffect(() => {
		fetch(`https://budget-tracker-be.herokuapp.com/api/categories/${id}`,{
            method: "GET",
            headers: {
                "Authorization" : `Bearer ${localStorage.getItem('token')}`
            }
        })
		.then( res => res.json())
		.then( data => {
			console.log(data)
            setCategory(data)
		})
		.catch( err => console.log(err))
    },[lastUpdatedCategory])

    console.log(category)
   
    return (
        <Container>
            <Row>
                <Col xs={6} sm={6} md={6} className="mx-auto">
                    <Card.Body>
                        <h1 className="categoryupdate">CATEGORY NAME: <u><em>{category.name}</em></u></h1>
                    </Card.Body>
                </Col>
                
            </Row>
                
            <Row>
            <Col xs={2} sm={2} md={2} className="mx-auto">
                        {
                            isLoading ?
                            <span className="visually-hidden"></span>
                            :

                            <CategoryListDisplay category= {category} />
                        }
                    </Col>
                    <Col>
                    <CategoryListUpdate category= {category} setLastUpdatedCategory= {setLastUpdatedCategory} />
                    </Col>
                   
            </Row>
        </Container>
    )

}