import { Redirect } from 'react-router-dom';
import { useState } from 'react';
import { Container, Row, Col } from 'react-bootstrap';

import LoginForm from './../components/LoginForm';



export default function Login() {

    //create useState for redirection
    const [ isRedirect, setIsRedirect ] = useState (false)

    return (
        // if is redirect true >> homepage
        isRedirect ?
        <Redirect to="/" /> :

        <Container>
            <Row>
                <Col>
                    <h1>Login</h1> 
                    <LoginForm setIsRedirect={setIsRedirect}/>
                </Col>
            </Row>
        </Container>
       
    )
}
