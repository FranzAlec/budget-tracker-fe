import { createContext, useState, useEffect } from 'react';


export const ApplicationContext = createContext()

export default function ApplicationProvider (props) {

    const [categories, setCategories] = useState([])

    const [ isUpdated, setIsUpdated ] = useState()
    
    const [users, setUsers] = useState([])

    const [newCategory, setNewCategory] = useState([
        {
            name:""
        }
    ])

    //function for add
    const setAdditionalCategory = (newcategory) =>{
	    setNewCategory([
	      ...newCategory,
	      newcategory
	    ])
	}


    const [newEntry, setNewEntry] = useState([
        {
            categoryId:"",
            description: "",
            amount: ""
        }
    ])

    //function for add entry
    const setAdditionalEntry = (newentry) =>{
	    setNewEntry([
	      ...newEntry,
	      newentry
	    ])
	}



    //fetch users
    useEffect (() =>{
    //    console.log("kahitano")
        
        fetch('https://budget-tracker-be.herokuapp.com/api/users', {
            headers: {
                'Authorization' : `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then (res => res.json())
        .then(data =>{
            setUsers(data)
        }) 
        .catch(err =>console.log(err))
    
    }, [])


    //fetch categories from localhost
    useEffect (() =>{
       
        if (!localStorage.token){

        } else {
        fetch('https://budget-tracker-be.herokuapp.com/api/categories', {
            headers: {
                'Authorization' : `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then (res => res.json())
        .then(data =>{
            // console.log('hello')
            setCategories(data)
        }) 
        .catch(err =>console.log(err))
    }
    }, [])

    


    return (
        <ApplicationContext.Provider
            value={{
                isUpdated,
                setIsUpdated,
                setAdditionalEntry,
                setAdditionalCategory,
                newCategory,
                setNewCategory,
                categories,
                setCategories,
                users,
                setUsers
               
            }}
        >
            {props.children}
        </ApplicationContext.Provider>
    )
}