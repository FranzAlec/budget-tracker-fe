import { Nav, Navbar } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { useContext } from 'react';
import { ApplicationContext } from './../contexts/AppContext';

export default function MainNav () {

    const { users, setUsers } = useContext(ApplicationContext)
     
    const handleClick = () =>{
        setUsers({
                userId: "",
                isAdmin: false,
                email: "",
                firstName: "",
                lastName: ""
        })
        localStorage.clear()
        alert("You are now logged out!")
    }

    const navLinks = !localStorage.token ?
    <>
        <Nav.Link as={Link} to="/login">Login</Nav.Link>
        <Nav.Link as={Link} to="/users">Register</Nav.Link>
    </> :
    <>
        <Nav.Link as={Link} to="/categories">My Categories</Nav.Link>
        <Nav.Link as={Link} to="/entries">My Entries</Nav.Link>
        <Nav.Link onClick={handleClick}>Logout</Nav.Link>
    </>
    return (
        <div>
        <Navbar bg="dark" variant="dark" expand="lg">
        <Navbar.Brand as={Link} to="/">Budget Tracker</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="ml-auto">
                    { navLinks }
                </Nav>
        </Navbar.Collapse>   
        </Navbar>
        </div>
    )
}