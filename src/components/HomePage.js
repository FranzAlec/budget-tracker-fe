
import img1 from "./../../src/images/money.png"

export default function HomePage (){


    return (

        <div className="home">
            <a className={"greet font-xl"}>Welcome to your<br></br>Budget Tracker</a> 
            <img src={img1} alt="Wallet" className="img1"></img>
        </div>

    )
}