import { Card, Button, Form } from 'react-bootstrap';
import { useState } from 'react';
import { Link } from 'react-router-dom';


export default function CategoryListUpdate ({category, setLastUpdatedCategory})  {

    const [currentCategory, setCurrentCategory] =useState ({})
    
    const handleChange = e =>{
        setCurrentCategory({
            ...currentCategory,
            [e.target.id] : e.target.value
        })
    }


    const handleSubmit = (e) =>{
        e.preventDefault()

        fetch(`https://budget-tracker-be.herokuapp.com/api/categories/${category._id}`,{
            method : "PUT",
            body: JSON.stringify(currentCategory),
            headers: {
                "Content-Type" : "application/json",
                "Authorization" : `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then( data => {
            setLastUpdatedCategory(data)
            alert("Category Updated Successfully")
            // console.log(data)
        })
        .catch(err => console.log(err))
    }


    return (

        <Card.Body>
            <Form onSubmit ={handleSubmit}>
            <Form.Group className="mb-3" controlId="name">
                <Form.Label className="categoryupd">UPDATE CATEGORY</Form.Label>
                <Form.Control 
                type="name" 
                placeholder="Enter category name"
                onChange={handleChange}
                 />
            </Form.Group>

            <Button className="mr-2" variant="primary" type="submit">
            Submit
            </Button>
            <Button as={Link} to="/categories" variant="secondary">Back to my Categories</Button>
            </Form> 
        </Card.Body>
    )
}