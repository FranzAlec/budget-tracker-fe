import { Button, Form } from 'react-bootstrap';
import { useState, useContext } from 'react';
import { ApplicationContext } from './../contexts/AppContext';
import { Link } from 'react-router-dom';


export default function EntryListUpdate ({entry, setLastUpdatedEntry})  {

    // console.log(entry)

    const [currentEntry, setCurrentEntry] =useState ({
        categoryId: '',
        description: '',
        amount: ''
    })

    const handleChange = e =>{
        // console.log(e.target.value)
        setCurrentEntry({
            ...currentEntry,
            [e.target.id] : e.target.value
        })
    }

    const {categories} = useContext(ApplicationContext)

    const {setIsUpdated} = useContext(ApplicationContext)

    const [ isLoading, setIsLoading ] =useState(false)


    const handleSubmit = (e) =>{
        e.preventDefault()
        console.log(entry._id)
        fetch(`https://budget-tracker-be.herokuapp.com/api/entries/${entry._id}`,{
            method : "PUT",
            body: JSON.stringify(currentEntry),
            headers: {
                "Content-Type" : "application/json",
                "Authorization" : `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res =>{
           console.log(res.json()) 
           return res.json()
        })
        .then( data => {
            setIsUpdated(data)
            alert("Entry Updated Successfully")
            // console.log(data)
        })
        .catch(err => console.log(err))
    }

        // console.log(categories)
        let categoryDisplay = categories.map((category, index) => {
            // console.log(category.name)
            return  <option value={category._id} key={index}>{category.name}</option>
        })
        
    

   
    console.log()

    return (

        <Form onSubmit ={handleSubmit}>
                

                <Form.Group controlId="categoryId">
                    <Form.Control 
                    as="select" 
                    onChange={handleChange}
                    > 
                        <option>Category</option>
                        {categoryDisplay}
                    </Form.Control>
                    
                </Form.Group>

                <Form.Group controlId="description">
                    <Form.Control 
                    type="text" 
                    placeholder="Enter Description"
                    onChange={handleChange}
                    className="mb-3"
                    /> 
                </Form.Group>

                <Form.Group controlId="amount">
                    <Form.Control 
                    type="number" 
                    placeholder="Enter Amount"
                    onChange={handleChange}
                    className="mb-3"
                    /> 
                </Form.Group>
            

       
                <Button className="ml-3" variant="primary" type="submit">Submit</Button>
            
                <Button className="ml-3" as={Link} to="/entries" variant="secondary">Back to my Entries</Button>
        </Form>
    )
}