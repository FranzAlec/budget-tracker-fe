import { Form, Button } from 'react-bootstrap';
import { useState } from 'react';



export default function RegistrationForm ({setIsRedirect}) {


   //user login info in useState
    const [ registration, setRegistration ] = useState ({
        firstName: "",
        lastName: "",
        email: "",
        password: "",
        confirmPassword: ""
    });


    //handling submit for registration
    const handleSubmit = (e) =>{
    e.preventDefault()

    //fetch path where the registration info will submit
    fetch('https://budget-tracker-be.herokuapp.com/api/users',{
        method: "POST",
        body: JSON.stringify(registration),
        headers: {
            "Content-Type": "application/json"
        }
        })
        .then(res => res.json())
        .then(data =>{
            alert("Registration Successful!")
            setIsRedirect(true)
            console.log(data)
        })
        .catch(err => console.log(err))
            // alert("Registration Failed!")
        //this resets the form
        setRegistration({
            firstName: "",
            lastName: "",
            email: "",
            password: "",
            confirmPassword: ""
        })
    }


     //handleChange on input
    const handleChange = (e) =>{
        setRegistration ({
            ...registration,
            [e.target.id] : e.target.value
            
        })
        console.log(e.target.value)
    };

    return (
        <Form onSubmit ={handleSubmit}>
            <Form.Group className="mb-3" controlId="firstName">
                <Form.Label>First Name</Form.Label>
                <Form.Control 
                type="firstName"
                onChange={handleChange}
                value={registration.firstName}
                />
            </Form.Group>

            <Form.Group className="mb-3" controlId="lastName">
                <Form.Label>Last Name</Form.Label>
                <Form.Control 
                type="lastName"
                onChange={handleChange}
                value={registration.lastName}
                />
            </Form.Group>

            <Form.Group className="mb-3" controlId="email">
                <Form.Label>Email Address</Form.Label>
                <Form.Control type="email"
                onChange={handleChange}
                value={registration.email}
                />
            </Form.Group>

            <Form.Group className="mb-3" controlId="password">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                type="password"
                onChange={handleChange}
                value={registration.password}
                />
            </Form.Group>

            <Form.Group className="mb-3" controlId="confirmPassword">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                type="password"
                onChange={handleChange}
                value={registration.confirmPassword}
                />
            </Form.Group>

            <Button type="submit" block>Sign Up</Button>
        </Form>
    )
}