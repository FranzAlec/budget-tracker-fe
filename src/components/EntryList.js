import EntryListDisplay from './../components/EntryListDisplay';
import { useState, useEffect, useContext } from 'react';
import { Col, Row, Container} from 'react-bootstrap';
import { ApplicationContext } from './../contexts/AppContext';




export default function EntryList () {

	const [entries, setEntries] = useState ([])

	const {isUpdated} = useContext(ApplicationContext)


	const[lastDeletedCategory, setLastDeletedCategory] = useState({})
	
	

    useEffect(() => {
		let access = localStorage.getItem("token");
		fetch("https://budget-tracker-be.herokuapp.com/api/entries",{
		  headers: {
			'Authorization' : `Bearer ${access}`
		  }
		})
		  .then((res) => res.json())
		  .then((data) => {
			setEntries(data)
		  })
	
		  .catch((err) => console.log(err));
	  }, [lastDeletedCategory,isUpdated]);

	  let balance = 0

	
    const entryDisplay = entries.map((entry,index)=>{
		

		balance += entry.amount

        return(  
		<Col xs={12} sm={6} md={4} lg={3} key={index}>
			<EntryListDisplay 
			entry={entry}
			setLastDeletedCategory={setLastDeletedCategory}
			
			/>
		</Col>  
	)

})

return (
	<Container className="my-1">
		<Row>
			<Col>
				<h1 className="balance">Current Available Balance: ${balance}</h1>
				<h2 className="mt-3">Entry List:</h2>
			</Col>
		</Row>
		<Row>
			{entryDisplay}
		</Row>
	</Container>
)
}