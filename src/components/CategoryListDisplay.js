import { Card, Button } from 'react-bootstrap';
// import { Link } from 'react-router-dom';
import { ApplicationContext } from './../contexts/AppContext';
import { useContext } from 'react'; 
import { Link } from 'react-router-dom';


export default function CategoryListDisplay ({category, setLastDeletedCategory}) {

    const { user, setIsUpdated } = useContext(ApplicationContext)
    const handleClick = () =>{
        fetch(`https://budget-tracker-be.herokuapp.com/api/categories/${category._id}`,{
            method: "DELETE",
            headers: {
                'Authorization' : `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then( res => res.json())
        .then ( data =>{
            // console.log(data)
            setIsUpdated(1)
        })
        .catch ( err => console.log(err))
    }

    return (
        <Card className="my-2">
            <Card.Body>
                <Card.Title>{category.name}</Card.Title>
                <Button as={Link} to={`/categories/${category._id}`} className="my-1" variant="primary" block>Edit</Button>
                
					<Button 
                    className="my-1" 
                    variant="danger" 
                    onClick={handleClick}
                    block>Delete</Button>
            </Card.Body>
        </Card>
    )
}