import { Button, Card } from 'react-bootstrap';
import { ApplicationContext } from './../contexts/AppContext';
import { useContext } from 'react'; 
import { Link } from 'react-router-dom';


export default function EntryListDisplay ({entry}) {

   
    const { user, setIsUpdated } = useContext(ApplicationContext)
    const handleClick = () =>{
        fetch(`https://budget-tracker-be.herokuapp.com/api/entries/${entry._id}`,{
            method: "DELETE",
            headers: {
                'Authorization' : `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then( res => res.json())
        .then ( data =>{
            // console.log(data)
            setIsUpdated(data)
        })
        .catch ( err => console.log(err))
    }
    
    return (
      
        <Card className="my-2">
            <Card.Body>
                <Card.Title>{entry.description}</Card.Title>
                <Card.Text variant="success">Amount: {entry.amount}</Card.Text>
                <Card.Text variant="success">Category Name: {entry.categoryId.name}</Card.Text>
                <Button as={Link} to={`/entries/${entry._id}`} className="my-1" variant="primary" block>Edit</Button>
               
					<Button 
                    className="my-1" 
                    variant="danger" 
                    onClick={handleClick}
                    block>Delete</Button>
				
				

                
            </Card.Body>
        </Card>
        
    )
}