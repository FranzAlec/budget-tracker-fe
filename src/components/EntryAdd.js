import { useState,useContext, useEffect } from 'react';
import { Form, Button  } from 'react-bootstrap';
import { ApplicationContext } from './../contexts/AppContext';


export default function CategoryAdd () {


    const [ entries, setEntries ] = useState({
        categoryId: '',
        description: '',
        amount: ''
    })

    const {categories, setIsUpdated} = useContext(ApplicationContext)

    const [ isLoading, setIsLoading ] =useState(false)


    let categoryDisplay = []
    if(!isLoading) {
        categoryDisplay = categories.map((category, index) => {
            return  <option value={category._id} key={index}>{category.name}</option>
            // console.log(category.name)
        })
    }

    

    //fetch entries from localhost
    useEffect (() =>{
       
        if (!localStorage.token){

        } else {
        fetch('https://budget-tracker-be.herokuapp.com/api/entries', {
            headers: {
                'Authorization' : `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then (res => res.json())
        .then(data =>{
            // console.log('hello')
            setEntries(data)
        }) 
        .catch(err =>console.log(err))
    }
    }, [])

   
    const handleSubmit = (e) =>{
        e.preventDefault()

        setIsLoading(true)

        //fetch path where the entries info will submit
        // let access = localStorage.getItem("token");
        fetch('https://budget-tracker-be.herokuapp.com/api/entries',{
            method: 'POST',
            body: JSON.stringify(entries),
            headers: {
                'Authorization' : `Bearer ${localStorage.getItem("token")}`,
                "Content-Type" : "application/json"
            }
            })
            .then(res => {
                setIsLoading(false)
                return res.json()
            })
            .then(data =>{
                setEntries (data)
                setIsUpdated(data)
                alert("Entry Added Successfully!")
            })
            .catch(err => console.log(err))
                // alert("Entry Adding Failed!")
            //this resets the form
            
        }

        
    


    const handleChange = (e) =>{
        // console.log(e.target.value)
        setEntries({
            ...entries,
            [e.target.id] : e.target.value
        })
    }

    return (
        <Form onSubmit ={handleSubmit}>
                <Form.Label className="categoryadd">ADD ENTRY</Form.Label>

                <Form.Group controlId="categoryId">
                    <Form.Control as="select" onChange={handleChange} value={entries.categoryId}> 
                        <option>Category</option>
                        {categoryDisplay}
                    </Form.Control>
                </Form.Group>

                <Form.Group controlId="description">
                    <Form.Control 
                    type="text" 
                    placeholder="Enter Description"
                    onChange={handleChange}
                    value={entries.description}
                    className="mb-3"
                    /> 
                </Form.Group>

                <Form.Group controlId="amount">
                    <Form.Control 
                    type="text" 
                    placeholder="Enter Amount"
                    onChange={handleChange}
                    value={entries.amount}
                    className="mb-3"
                    /> 
                </Form.Group>
            

       
            { isLoading ?
                <Button variant="primary" type="submit" disabled>Submit</Button>
               : <Button variant="primary" type="submit">Submit</Button>
            }

        </Form>
    );
}
