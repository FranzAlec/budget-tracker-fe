import { useState, useContext, useEffect } from 'react';
import { Form, Button } from 'react-bootstrap';
import { ApplicationContext } from './../contexts/AppContext';



export default function CategoryAdd () {

    const context = useContext(ApplicationContext)

    const {isUpdated} = useContext(ApplicationContext)

    const [ addCategory, setAddCategory ] = useState ({
        name: "",
    });

    const [ isLoading, setIsLoading ] =useState(false)



    useEffect (() =>{
       
        if (!localStorage.token){

        } else {
        fetch('https://budget-tracker-be.herokuapp.com/api/categories', {
            headers: {
                'Authorization' : `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then (res => res.json())
        .then(data =>{
            context.setNewCategory(data)
        }) 
        .catch(err =>console.log(err))
    }
    },[isUpdated])





    const handleSubmit = (e) =>{
        e.preventDefault()

        //fetch path where the registration info will submit
        setIsLoading(true)
        // let access = localStorage.getItem("token");
        fetch('https://budget-tracker-be.herokuapp.com/api/categories',{
            method: 'POST',
            body: JSON.stringify(addCategory),
            headers: {
                'Authorization' : `Bearer ${localStorage.getItem("token")}`,
                "Content-Type" : "application/json"
            }
        })
            .then(res => {
                // console.log(res)
                setIsLoading(false)
                return res.json()
            })
            .then(data =>{
                // console.log(data)
                alert("Category Added Successfully!")
                context.setAdditionalCategory(data)
                
            })
            .catch(err => console.log(err))
                // alert("Category Adding Failed!")
            //this resets the form
            setAddCategory({
              name: ""
            })
        
        }



    const handleChange = (e) =>{
        setAddCategory({
            name : e.target.value
        })
        // console.log(ame)
    }

    return (
        
        <Form onSubmit ={handleSubmit}>
            <Form.Group className="mb-3" controlId="name">
                <Form.Label className="categoryadd">ADD CATEGORY</Form.Label>
                <Form.Control 
                type="name" 
                placeholder="Enter category name"
                onChange={handleChange}
                />
            </Form.Group>

            <Button className="mb-5" variant="primary" type="submit">
            Submit
            </Button>
        </Form>
    )
    }

