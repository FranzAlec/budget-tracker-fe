import CategoryListDisplay from './../components/CategoryListDisplay';
import { useState, useEffect, useContext } from 'react';
import { Col, Row, Container} from 'react-bootstrap';
import {ApplicationContext} from './../contexts/AppContext';


export default function CategoryList () {

	const [categories, setCategories] = useState ([])

	const context = useContext(ApplicationContext)

	const {isUpdated} = useContext(ApplicationContext)


	const[lastDeletedCategory, setLastDeletedCategory] = useState({})

	

    useEffect(() => {
		let access = localStorage.getItem("token");
		fetch("https://budget-tracker-be.herokuapp.com/api/categories",{
		  headers: {
			Authorization: `Bearer ${access}`
		  }
		})
		  .then((res) => res.json())
		  .then((data) => {
			setCategories(data);
	
		  })
	
		  .catch((err) => console.log(err));
	  }, [lastDeletedCategory, isUpdated]);
	


    const categoryDisplay = context.newCategory.map((category,index)=>{
		
        return(  
		<Col xs={12} sm={6} md={4} lg={3} key={index}>
			<CategoryListDisplay 
			category={category}
			setLastDeletedCategory={setLastDeletedCategory}
			
			/>
		</Col>  
	)

})

return (
		<Container className="my-1">
			<h2>Category List:</h2>
			<Row>
				{categoryDisplay} 
			</Row>
		</Container>
)
}