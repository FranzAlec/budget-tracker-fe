import { Form, Button } from 'react-bootstrap';
import { useState, useContext } from 'react';
import { ApplicationContext } from '../contexts/AppContext';

export default function LoginForm ({setIsRedirect}) {

    //enable setUser from the appcontext
    const { setUsers } = useContext(ApplicationContext)

    //user login info
    const [ credentials, setCredentials ] = useState ({
        email: "",
        password: ""
    })
    
    //for a loading time
    const [ isLoading, setIsLoading ] = useState(false)


    //handleSubmit once submitted on form
    const handleSubmit = (e) => {
        e.preventDefault()
        setIsLoading(true)

        //fetch user login url
        fetch('https://budget-tracker-be.herokuapp.com/api/users/login', {
            method: "POST",
            body: JSON.stringify (credentials),
            headers: {
                "Content-Type" : "application/json"
            }
        })
        .then(res => {
            //to check the response object after submitting
            // console.log(res)
            setIsLoading(false)

            if (res.status === 200){
                alert("Login Successful")
                return res.json()
            } else {
                alert("Invalid Credentials")
                throw new Error ("Invalid Credentials")
            }
        })

        .then(token => {
            let access = token.token
            //shows token in console
            // console.log(access)
            localStorage.setItem("token", access)
            //return the fetched user
            return fetch ('https://budget-tracker-be.herokuapp.com/api/users', {
                headers : {
                    'Authorization' : `Bearer ${access}`
                }
            })
        })
        .then( res => res.json())
        .then( data =>{
            //call the setUser
            const { firstName, lastName, email, isAdmin } = data
            setUsers({
                userId: data._id,
                firstName,
                lastName,
                email,
                isAdmin
            })
            setIsRedirect(true)
        })
        .catch(err => console.log(err))
    }

    //handleChange on input
    const handleChange = (e) =>{
        setCredentials ({
            ...credentials,
            [e.target.id] : e.target.value
            
        })
        //to check the e.target.value on input
        // console.log(e.target.value)
    }

    return (
        <Form onSubmit={handleSubmit}>
            <Form.Group className="mb-3" controlId="email">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                    type="email" 
                    placeholder="Enter email" 
                    onChange={handleChange}
                    value={credentials.email}
                />
                <Form.Text className="text-muted">
                We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group className="mb-3" controlId="password">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Password" 
                    onChange={handleChange}
                    value={credentials.password}
                />
            </Form.Group>

            <Button variant="primary" type="submit" block>
            Submit
            </Button>
        </Form>
    )
}
