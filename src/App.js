import './App.css'

import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import MainNav from './components/MainNav';
import ApplicationProvider from './contexts/AppContext';
import Category from './pages/Category';
import Home from './pages/Home';
import Login from './pages/Login';
import Register from './pages/Register';
import Entry from './pages/Entry';
import CategorySingle from './pages/CategorySingle';
import EntrySingle from './pages/EntrySingle';


function App() {

  return (
    <div className="App">
      <ApplicationProvider>
      <Router>
        <MainNav />
        <Switch>
          <Route exact path="/">
            <Home />
          </Route>
          <Route path="/login">
            <Login />
          </Route>
          <Route path="/users">
            <Register />
          </Route>
          <Route exact path="/categories">
            <Category />
          </Route>
          <Route exact path="/entries">
            <Entry />
          </Route>
          <Route  path="/categories/:id">
            <CategorySingle />
          </Route>
          <Route  path="/entries/:id">
            <EntrySingle />
          </Route>
        </Switch>
      </Router>
      </ApplicationProvider>
    </div>
  );
}

export default App;
